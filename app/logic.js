const {
    ApplicationType,
    assert,
    AttributeIds,
    BrowseDirection,
    callConditionRefresh,
    ClientMonitoredItem,
    coerceMessageSecurityMode,
    coerceNodeId,
    coerceSecurityPolicy,
    constructEventFilter,
    dumpEvent,
    hexDump,
    makeExpandedNodeId,
    makeNodeId,
    MessageSecurityMode,
    NodeClassMask,
    NodeCrawler,
    NodeId,
    ObjectTypeIds,
    OPCUAClient,
    resolveNodeId,
    SecurityPolicy,
    VariableIds,
    ObjectIds,
    UserTokenType,
} = require("node-opcua");
const chalk = require("chalk");

// const endpointUrl = "opc.tcp://localhost:26543";
const endpointUrl = "opc.tcp://10.101.207.6:49320";

const nodes_to_monitor = [
    "ns=2;s=SE.DSP.CS3_RWP",
    "ns=2;s=BUMP1.UA.CS4.CS4.CS4_RWP",
    "ns=2;s=BUMP1.UA.CS5.CS5.CS5_RWP",
    "ns=2;s=BUMP1.UA.CS6.CS6.CS6_RWP",
    "ns=2;s=BUMP1.UA.CS7.CS7.CS7_RWP",
    "ns=2;s=BUMP1.UA.DFA.DFA.DFA_RWP",
    // "ns=2;s=BUMP1.UA.EAS.EAS.EAS_RWP",
    "ns=2;s=BUMP1.UA.EAS.EAS.BMS_RWP",
    "ns=2;s=BUMP1.UA.HDB_MOD.HDB_MOD.HDB_RWP",
    "ns=2;s=BUMP1.UA.KIN.KIN.KIN_RWP",
    "ns=2;s=BUMP1.UA.NUR.NUR.NUR_RWP",
    "ns=2;s=BUMP1.UA.PPL.PPL.PPL_RWP",
    "ns=2;s=BUMP1.UA.TSC_EXC.TSC_EXC.TSC_RWP",
    "ns=2;s=BUMP1.UA.SEA.SEA.SEA_RWP"
];

var node_values = {};
exports.latest = function(callback){
    callback(node_values);
}

const securityMode = coerceMessageSecurityMode("None");
const securityPolicy = coerceSecurityPolicy("None");
console.log(chalk.cyan("securityMode        = "), securityMode.toString());
console.log(chalk.cyan("securityPolicy      = "), securityPolicy.toString());
var monitored_nodes = [];
nodes_to_monitor.forEach(function(node_id){
	monitored_nodes.push(coerceNodeId(node_id));
})
async function main() {
    const options = {
        securityMode,
        securityPolicy,
        defaultSecureTokenLifetime: 40000,
        endpointMustExist: false,
        connectionStrategy: {
            initialDelay: 2000,
            maxDelay: 10 * 1000,
            maxRetry: 10
        }
    };
    client = OPCUAClient.create(options);
    console.log("connecting to ", chalk.cyan.bold(endpointUrl));
    await client.connect(endpointUrl);
    console.log(" Connected ! exact endpoint url is ", client.endpointUrl);
    the_session = await client.createSession();
    client.on("connection_reestablished", () => {
        console.log(chalk.bgWhite.red(" !!!!!!!!!!!!!!!!!!!!!!!!  CONNECTION RE-ESTABLISHED !!!!!!!!!!!!!!!!!!!"));
    });
    console.log(chalk.yellow(" session created"));
    console.log(" sessionId : ", the_session.sessionId.toString());

    client.on("backoff", (retry, delay) => {
        console.log(chalk.bgWhite.yellow("backoff  attempt #"), retry, " retrying in ", delay / 1000.0, " seconds");
    });
    client.on("start_reconnection", () => {
        console.log(chalk.bgWhite.red(" !!!!!!!!!!!!!!!!!!!!!!!!  Starting Reconnection !!!!!!!!!!!!!!!!!!!"));
    });
    const parameters = {
        maxNotificationsPerPublish: 10,
        priority: 10,
        publishingEnabled: true,
        requestedLifetimeCount: 1000,
        requestedMaxKeepAliveCount: 12,
        requestedPublishingInterval: 2000
    };
    the_subscription = await the_session.createSubscription2(parameters);
    console.log("started subscription :", the_subscription.subscriptionId);
    console.log(" revised parameters ");
    console.log("  revised maxKeepAliveCount  ", the_subscription.maxKeepAliveCount, " ( requested ", parameters.requestedMaxKeepAliveCount + ")");
    console.log("  revised lifetimeCount      ", the_subscription.lifetimeCount, " ( requested ", parameters.requestedLifetimeCount + ")");
    console.log("  revised publishingInterval ", the_subscription.publishingInterval, " ( requested ", parameters.requestedPublishingInterval + ")");
    console.log("subscription duration ",
        ((the_subscription.lifetimeCount * the_subscription.publishingInterval) / 1000).toFixed(3), "seconds"
    )
    the_subscription.on("internal_error", (err) => {
        console.log(" received internal error", err.message);
    }).on("keepalive", () => {
        const t4 = getTick();
        const span = t4 - t;
        t = t4;
        console.log("keepalive ", span / 1000, "sec",
            " pending request on server = ", the_subscription.getPublishEngine().nbPendingPublishRequests);
    }).on("terminated", () => { /* */
        console.log("Subscription is terminated ....")
    });
    for(monitored_node of monitored_nodes){
	    const monitoredItem = ClientMonitoredItem.create(
	        the_subscription,
	        {
	            attributeId: AttributeIds.Value,
	            nodeId: monitored_node
	        },
	        {
	            discardOldest: true,
	            queueSize: 10000,
	            samplingInterval: 1000
	            // xx filter:  { parameterTypeId: "ns=0;i=0",  encodingMask: 0 },
	        }
	    );
	    monitoredItem.on("initialized", () => {
	        console.log("monitoredItem initialized");
	    });
	    monitoredItem.on("changed", (dataValue1) => {
            let node_id = monitoredItem.itemToMonitor.nodeId;
            let node_value = dataValue1.value.value;
            console.log(node_id + ": " + node_value);
            node_values[node_id] = node_value; 
	    });
	    monitoredItem.on("err", (err_message) => {
	        console.log(monitoredItem.itemToMonitor.nodeId.toString(), chalk.red(" ERROR"), err_message);
	    });
    }
    const results = await the_subscription.getMonitoredItems();
}
main();