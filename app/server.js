var express = require('express');
var app = express(); 
var logic = require("./logic");
const nocache = require('nocache');

app.use(nocache());
app.set('port', 8081);

app.get('/', function (req, res) {
	res.header('Access-Control-Allow-Origin' , '*');
	res.send('Server is on line');
})

app.get('/latest', function (req, res) {
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	logic.latest(callback); 
})

var server = app.listen(app.get('port'), function() {
  var port = server.address().port;
  console.log('http happens on port ' + port);
});